# Guia de Estilos
Durante el desarrollo de nuestros proyectos, procuramos tener un estilo "unificado", esto con la finalidad de tener una ambiente de desarrollo homogeneo entre los desarrolladores; por esto te invitamos a seguir las siguientes guias de estilo.

## Formaters
Dentro de nuestros proyectos utilizamos el formateador de código [black](https://github.com/psf/black), con la bandera de `--line-length 100`.
Además de esto, utilizamos `isort` para el orden de nuestros imports, con la siguiente gerarquia: `"FUTURE, STDLIB, THIRDPARTY, FIRSTPARTY, LOCALFOLDER"`.

## Linterns
Como linterns utilizamos [pylint](https://pylint.org/) y [mypy](http://mypy-lang.org/). Ten en cuenta que deshabilitamos algunas banderas de pylint con el fin de agilziar el desarrollo, por ejemplo las banderas que forzan a crear docstrings dentro de un módulo o que limitan la creación de comentarios del tipo `TODO`.

## Pre-commit
Nuestros proyectos utilizan la herramienta [pre-commit](https://pre-commit.com/) para "asegurar" que el código cumpla con nuestras especificaciones, con esto no tienes que preocuparte mucho de recordar estos estándares que se mencionan, basta con que instales pre-commit dentro de tu repositorio y que lo habilites.
```shell
pip install pre-commit
pre-commit install
```
Esto se encargará de correr los procesos de formating y linting automáticamente cada que realices un commit.
