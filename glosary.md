# Glosario

## CFDI
*Comprobante Fiscal Digital por Internet* ([CFDI](https://www.cfdi.org.mx/))

## pre-CFDI
Se le conoce con este nombre a los XML's listos para ser enviados al SAT o al PAC para convertirse en un CFDI oficial, es decir, es el paso previo al momento del timbrado.
