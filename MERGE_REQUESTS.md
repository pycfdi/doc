# Merge Requests
Considera las siguientes directrices antes de realizar un *Merge Requests*:

* Usa una rama única que se desprenda de la rama principal, utiliza algún nombre relacionado a la funcionalidad a implementar. Ej: `feature_add_certificate_sign_method`, `fix_private_key_leak`
* No mezcles dos diferentes funcionalidades en una misma rama o *Merge Request*.
* Describe claramente y en detalle los cambios que hiciste.
* **Escribe pruebas** para la funcionalidad que deseas agregar.
* **Asegúrate que las pruebas pasan** antes de enviar tu contribución.
  Usamos integración continua donde se hace esta verificación, pero es mucho mejor si lo pruebas localmente.
* Intenta enviar una historia coherente, entenderemos cómo cambia el código si los *commits* tienen significado.
* La documentación es parte del proyecto.
  Realiza los cambios en los archivos de ayuda para que reflejen los cambios en el código.
