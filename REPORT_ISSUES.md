# Reportar problemas
Antes de reportar un problema, sigue las siguientes recomendaciones:

1. Asegurate de que el problema sea real. Realiza algún test unitario, o ten claro un escenario dónde el problema se pueda reproducir consitentemente.
2. Revisa en la comunidad o en los *Merge Requests* existentes para ver si el problema no ha sido ya reportado con aterioridad.
3. Una vez confirmado el bug y sabiendo que no está reportado, genera un *Issue* dentro del proyecto en cuestión.

## Generación de un *Issue*
Al generar un nuevo Issue, sigue las siguientes recomendaciones:
1. Incluye la descripción del problema, siendo lo más claro y conciso posible.
2. Indica bajo que escenario es el que se presenta el problema.
3. Indica cuál es el resultado actual obtenido y cuál sería el esperado.
4. Si te es posible, propon un solución (no necesariamente cómo código, sino como idea o camino a seguir por los desarrolladores del proyecto).
5. Si te es posible, contribuye con una prueba unitaria o script donde se pueda reproducir el problema.
