# Contribuciones

Las contribuciones en cualquiera de los subproyectos de este macroproyecto son más que bienvenidas.
Este proyecto (y los que contiene) se apegan al código de conducta [Contributor Covenant](https://www.contributor-covenant.org/version/2/1/code_of_conduct/).
Al participar en este proyecto y en su comunidad, aceptas seguir este código.

## Miembros del equipo

* [pyCFDI](https://gitlab.com/pycfdi) - Organización principal.

## Canales de comunicación

Puedes encontrar ayuda y comentar asuntos relacionados con este proyecto en estos lugares:

* Comunidad Discord: <https://discord.gg/Tp7dkaQ82R>

## Reportar Bugs

Con la finalidad de mantener la organización, los errores, bugs, propuestas de contribución y cualquier otro tema asociado a un proyecto en concreto, se pide que se genere su propio Issue o Merge request (según sea el caso) en el mismo subproyecto.

Recuerda seguir estas [recomendaciones](REPORT_ISSUES.md) para el reporte de errores.

**Recuerda no incluir contraseñas, información personal o confidencial.**

## Corrección de Bugs

Apreciamos mucho los *Merge Requests* para corregir Bugs.

Si encuentras un reporte de Bug y te gustaría solucionarlo siéntete libre de hacerlo.
Sigue las siguientes [directrices](MERGE_REQUESTS.md) al momento de hacer tu aportación.

## Agregar nuevas funcionalidades

Si tienes una idea para una nueva funcionalidad revisa primero si existen discusiones o *Merge Requests*
en donde ya se esté trabajando en dicha funcionalidad.

Antes de trabajar en la nueva característica, utiliza los [Canales de comunicación](#canales-de-comunicacion) mencionados
anteriormente para platicar acerca de tu idea. Si dialogas tus ideas con la comunidad y los
mantenedores del proyecto, podrás ahorrar mucho esfuerzo de desarrollo y prevenir que tu
*Merge Request* sea rechazado. No nos gusta rechazar contribuciones, pero algunas características
o la forma de desarrollarlas puede que no estén alineadas con el proyecto.

## Entorno de desarrollo
La mayoría de nuestros proyectos siguen la siguiente estructura:
```shell
repo
    - doc
    - src
        - PYTHON_PACKAGES
    - tests
        - PYTHON_TESTS (pytest)
    - README.md
    - LICENSE
    - .gitlab-cy.yml
    - pyproject.toml
    - requirements.txt
    - poetry.lock
```
Como podras observar, utilizamos [Poetry](https://python-poetry.org) para la gestión de nuestros proyectos, se recomienda utilizar la misma herramienta para fines prácticos, sin embargo, también contamos con el archivo `requirements.txt` para instalaciones más "nativas"

### Instalación
Para instalar nuestros proyectos, ejecuta el siguiente comando dentro del repositorio correspondiente:
```shell
poetry install
```
Esto instalara todas las dependencias (incluyendo las de desarrollo, si no deseas estas últimas, añade la bandera `--no-dev`)

### Pruebas Unitarias
Para lanzar las pruebas unitarias, ejecuta el siguiente comando dentro del repositorio:
```shell
poetry run pytest tests
```

### Desarrollo de una nueva funcionalidad o reparación de un bug
Es importante demostrar que la nueva funcionalidad o bug están siendo correctamente implementados, para esto utilizamos la métodología de Red-Green-Refactor o Test Driven Development ([TDD](https://en.wikipedia.org/wiki/Test-driven_development)). Teniendo esto en cuenta, se solicita los colaboradores realicen el siguiente proceso para la aportación de nuevas carácteristicas.

1. Clonar el repositorio en local
2. Crear una o multiples pruebas unitarias dónde se demuestre que ACTUALMENTE no se cuenta cubierta esa funcionalidad o que se registré que el bug existe (La prueba debe fallar).
3. Realizar un commit que contenga esa prueba.
4. Realizar la implementación que haga que la prueba unitaria pase exitosamente.
5. Si es necesario, refactorizar el código, asegurarse de que la prueba siga siendo exitosa.
6. Realizar el o los commits con la solución.
7. Generar la Merge Request y revisar que el pipeline se efectue correctamente.
8. Esperar la aprobación y subsequente mezcla del cambio en la rama `main`.

**Nota**: Revisar la [Guia de estilo](STYLE_GUIDE.md) para asegurar que el código se apega al estilo de la comunidad.
**Nota**: Recuerda que utilizamos [Semantic Versioning](https://semver.org/) para la gestion de nuestros proyectos, por lo que es importante que tus cambios no rompan la compatibilidad del API pública con versiones previas (de ser así, especifica el porqué de este cambio "disruptivo").

### Contribuciones "no técnicas"
Sientete libre de contribuir no solamente con código, puedes apoyar en alguna de las siguientes secciones:
- Generar nueva o mejor documentación.
- Generar material de apoyo a la comunidad.
- Proponer nuevas funcionalidades o necesidades de los proyectos.
- Difundir la existencia de la comunidad.
- Cualquier otro apoyo que creas conveniente.
