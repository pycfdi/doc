# Roadmap
Aquí se listan los proyectos que nos interesa generar proximamente:

- [ ] **SAT WS Consumer**: Creación, revisión de estatus y descarga de peticiones de Consulta Másivas de CFDI's al WS del SAT.
- [ ] **Comprobación de EFOS**: Consulta "rápida" del estatus de un RFC para determinar su situación con respecto al listado [69-B](http://omawww.sat.gob.mx/cifras_sat/Paginas/datos/vinculo.html?page=ListCompleta69B.html).
- [ ] **Generador de pre-CFDI**: Herramienta para generar el XML de un [pre-CFDI](glosary.md#pre-cfdi) a partir de su información "básica" en formato JSON.
- [ ] **Conversor de CFDI a JSON**: Herramienta para, a partir de un pre-CFDI o CFDI, generar un JSON con la información del mismo.
