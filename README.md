# pyCFDI/Doc

En este repositorio se encuentra la documentación global de los proyectos dentro de la comunidad `pyCFDI`.
Sientete libre de navegar por este repositorio y sobre todo interactuar con la comunidad (levantar nuevos tickets de Issue, generar Merge Requests, realizar consultas dentro de los canales de comunicación, etc.).

Un buen punto de inicio es el archivo [CONTRIBUTING.md](CONTRIBUTING.md)

También puedes revisar nuestro [ROADMAP](ROADMAP.md) para descubrir cuales proyectos ya tenemos en mente implementar.
